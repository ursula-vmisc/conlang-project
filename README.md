version:	0.2021.0.0.0

objective:	to develope a feasible universal language for the current time/date

sources:	*

constraints:
		- must be easy to pronounce (by p% of people)
		- must be easy to write (by p% of people)
		- must be easy to type (by p% of people)
		- must have fluid grammatical structure

version: 	simple record system

		0.2021.0.0.0
		|   |  | | |
		----+--+-+-+--	branch version
		    |  | | |
		    ---+-+-+--	year of record
		       | | |
		       --+-+--	month of record
			 | |
			 --+--	day of record
			   |
			   ---	second of day
				; or, instance number (decimal or base 36)
				; for if there are multiple entries on the same day

copyright:	CC0